/**
 * jQuery should be accessed through $ by passing the jQuery object into an anonymous function.
 * @see {@link https://developer.wordpress.org/coding-standards/wordpress-coding-standards/javascript/#common-libraries}
 */
(function ($, window, document, undefined) {
	// At the top of the file, set "wp" to its existing value (if present)
	window.wp = window.wp || {};

	// misc page functions
	const pagedoc = {
		remove_empty_paragraph_element() {	// remove empty paragraphs			
			var n = $('p:empty').length;
			$('p:empty').remove();
			console.info(n+' empty paragraphs removed from the DOM.')
		},
	};

  // The document ready event executes when the HTML-Document is loaded
  // and the DOM is ready.
  jQuery(document).ready(function( $ ) {

		// remove empty p elements
		pagedoc.remove_empty_paragraph_element();

    // hide the quicklinks button
    $('button.uw-quicklinks')
      .prop('hidden', true)
      .addClass('hide');

    // remove empty deeper-dive sections
    $('.home .su-posts-uw-context-loop').each(function(){
      if ( $(this).children('.su-post').length == 0 ) {
        $(this).remove();
      }
    });
    
  })

  // The window load event executes after the document ready event,
  // when the complete page is fully loaded.
  jQuery(window).load(function () {

    // remove 'phantom' wordpress p elements
    $('.home .su-posts-uw-card-loop p, .home .su-posts-uw-rest-loop p').each(function(){
      if ($(this).is(':empty')) {
        $(this).remove();
      }
    });

  })

})(jQuery, this, this.document);
