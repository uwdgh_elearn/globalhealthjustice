<?php defined( 'ABSPATH' ) || exit; ?>

<?php
/**
 * READ BEFORE EDITING!
 *
 * Do not edit templates in the plugin folder, since all your changes will be
 * lost after the plugin update. Read the following article to learn how to
 * change this template or create a custom one:
 *
 * https://getshortcodes.com/docs/posts/#built-in-templates
 */
?>

<div class="su-posts su-posts-default-loop su-posts-uw-card-loop">

	<?php if ( $posts->have_posts() ) : ?>

		<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

			<?php
				// get custom field byline_author
				$byline_author = esc_attr( get_post_meta(get_the_ID(), 'byline_author', true) );
			?>

			<div id="su-post-<?php the_ID(); ?>" class="widget cards-widget su-post">
				<div class="widget-content">
					<div class="default-card" style="max-height: auto;">
						<?php if ( has_post_thumbnail( get_the_ID() ) ) : ?>
							<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID());  ?>
							<div><a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php the_title(); ?>"><div class="card-image" style="background-image:url(<?php echo esc_url($featured_img_url); ?>)"></div></a></div>
						<?php else: ?>
							<div><a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php the_title(); ?>"><div class="card-image" style="background-image:url(<?php echo esc_url(get_stylesheet_directory_uri().'/assets/png/image-placeholder.png'); ?>)"></div></a></div>
						<?php endif; ?>
						<h3 style="text-transform: none;">
							<a href="<?php the_permalink(); ?>" class="pic-title"><?php the_title(); ?></a><br>
						</h3>
						<?php if ( $byline_author ) : ?>
							<p><small><em>By <?php echo $byline_author; ?></em></small></p>
						<?php else: ?>
							<p><small><em>By <?php get_the_author(); ?></em></small></p>
						<?php endif; ?>
						<p><?php echo globalhealthjustice_get_excerpt(128); ?></p>
						<p><a href="<?php the_permalink(); ?>" class="pic-text-more uw-btn btn-sm btn-gold">Read more</a><br></p>
					</div>
				</div>
			</div>

		<?php endwhile; ?>

	<?php else : ?>
		<output class="small"><?php _e( 'No posts listed', 'shortcodes-ultimate' ); ?></output>
	<?php endif; ?>

</div>
