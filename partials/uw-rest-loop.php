<?php defined( 'ABSPATH' ) || exit; ?>

<?php
/**
 * READ BEFORE EDITING!
 *
 * Do not edit templates in the plugin folder, since all your changes will be
 * lost after the plugin update. Read the following article to learn how to
 * change this template or create a custom one:
 *
 * https://getshortcodes.com/docs/posts/#built-in-templates
 */
?>

<div class="su-posts su-posts-teaser-loop su-posts-uw-rest-loop">
	<?php if ( $posts->have_posts() ) : ?>
		<h4 class="uw-rest-loop-heading">Read also:</h4>
		<?php while ( $posts->have_posts() ) : ?>
			<?php $posts->the_post(); ?>
			<?php
			$is_deeper_dive = get_post_meta(get_the_ID(), 'deeper_dive', true);
			?>
			<?php if ( !$is_deeper_dive ) : ?>
			<div id="su-post-<?php the_ID(); ?>" class="su-post">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</div>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php else : ?>
		<!--p class="su-posts-not-found"><?php //esc_html_e( 'Posts not found', 'shortcodes-ultimate' ); ?></p-->
	<?php endif; ?>
</div>
