<?php defined( 'ABSPATH' ) || exit; ?>

<?php
/**
 * READ BEFORE EDITING!
 *
 * Do not edit templates in the plugin folder, since all your changes will be
 * lost after the plugin update. Read the following article to learn how to
 * change this template or create a custom one:
 *
 * https://getshortcodes.com/docs/posts/#built-in-templates
 */
?>

<div class="su-posts su-posts-teaser-loop su-posts-uw-blogroll-loop">
	<?php if ( $posts->have_posts() ) : ?>
		<div class="gallery js-flickity" data-flickity-options='{ "wrapAround": false, "cellAlign": "left" }'>
		<?php while ( $posts->have_posts() ) : ?>
			<?php $posts->the_post(); ?>
			<?php
				// get custom field byline_author
				$byline_author = esc_attr( get_post_meta(get_the_ID(), 'byline_author', true) );
			?>
			<div class="gallery-cell">
				<div id="su-post-<?php the_ID(); ?>" class="su-post widget cards-widget">
					<div class="widget-content">
						<div class="boundless-card">
							<?php if ( has_post_thumbnail( get_the_ID() ) ) : ?>
								<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID());  ?>
								<a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php the_title(); ?>"><div class="card-image" style="background-image:url(<?php echo esc_url($featured_img_url); ?>)"></div></a>
							<?php else: ?>
								<a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>" title="<?php the_title(); ?>"><div class="card-image" style="background-image:url(<?php echo esc_url(get_stylesheet_directory_uri().'/assets/png/image-placeholder.png'); ?>)"></div></a>
							<?php endif; ?>
							<h3 style="text-transform: none;">
								<a href="<?php the_permalink(); ?>" class="pic-title"><?php the_title(); ?></a><br>
							</h3>
							<?php if ( $byline_author ) : ?>
								<p><small><em>By <?php echo $byline_author; ?></em></small></p>
							<?php else: ?>
								<p><small><em>By <?php get_the_author(); ?></em></small></p>
							<?php endif; ?>
							<p><a href="<?php the_permalink(); ?>" class="pic-text-more uw-btn btn-sm btn-gold">Read more</a><br></p>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
	<?php else : ?>
		<!--p class="su-posts-not-found"><?php //esc_html_e( 'Posts not found', 'shortcodes-ultimate' ); ?></p-->
	<?php endif; ?>
</div>
