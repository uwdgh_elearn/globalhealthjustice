    <div role="contentinfo" class="uw-footer">

        <a href="https://www.washington.edu" class="footer-wordmark" title="<?php _e('University of Washington', 'uwdgh'); ?> Home"><?php _e('University of Washington', 'uwdgh'); ?></a>

        <a hidden class="hide" href="https://www.washington.edu/boundless/"><h3 class="be-boundless"><?php _e('Be boundless', 'uwdgh'); ?></h3></a>

        <h4><?php _e('Connect with Global Health Justice', 'uwdgh'); ?>:</h4>

        <nav role="navigation" aria-label="social networking">
            <ul class="footer-social">
                <li hidden class="hide"><a class="facebook" href="https://www.facebook.com/UWDGH/">Facebook</a></li>
                <li><a class="twitter" href="https://twitter.com/uwhealthjustice">Twitter</a></li>
                <li><a class="instagram" href="https://www.instagram.com/globalhealthjustice/">Instagram</a></li>
                <li><a class="youtube" href="https://www.youtube.com/channel/UCyqV13nxl7dcSsT7Oc4Vtlw">YouTube</a></li>
                <li><a class="linkedin" href="https://www.linkedin.com/company/global-health-justice">LinkedIn</a></li>
            </ul>
        </nav>
        <nav hidden class="hide" role="navigation" aria-label="footer links dgh">
            <ul class="footer-links">
                <li><a href="https://globalhealth.washington.edu/contact"><?php _e('Contact Us', 'uwdgh'); ?></a></li>
                <li><a href="https://globalhealth.washington.edu/about-us/jobs"><?php _e('Jobs', 'uwdgh'); ?></a></li>
                <li><a href="https://globalhealth.washington.edu/events"><?php _e('Events', 'uwdgh'); ?></a></li>
                <li><a href="https://globalhealth.washington.edu/news"><?php _e('News', 'uwdgh'); ?></a></li>
                <li><a href="https://globalhealth.washington.edu/intranet" target="_blank"><?php _e('Intranet', 'uwdgh'); ?></a></li>
                <li><a href="https://globalhealth.washington.edu/support-us"><?php _e('Donate', 'uwdgh'); ?></a></li>
            </ul>
        </nav>
        <nav role="navigation" aria-label="footer links uw">
            <ul class="footer-links">
                <li><a href="https://www.washington.edu/accessibility/"><?php _e('Accessibility', 'uwdgh'); ?></a></li>
                <li hidden class="hide"><a href="https://uw.edu/home/siteinfo/form"><?php _e('Contact the UW', 'uwdgh'); ?></a></li>
                <li hidden class="hide"><a href="https://www.washington.edu/jobs"><?php _e('Jobs', 'uwdgh'); ?></a></li>
                <li hidden class="hide"><a href="https://www.washington.edu/safety"><?php _e('Campus Safety', 'uwdgh'); ?></a></li>
                <li hidden class="hide"><a href="https://myuw.washington.edu/"><?php _e('My UW', 'uwdgh'); ?></a></li>
                <li hidden class="hide"><a href="https://www.washington.edu/rules/wac"><?php _e('Rules Docket', 'uwdgh'); ?></a></li>
                <li><a href="https://www.washington.edu/online/privacy/"><?php _e('Privacy', 'uwdgh'); ?></a></li>
                <li><a href="https://www.washington.edu/online/terms/"><?php _e('Terms', 'uwdgh'); ?></a></li>
            </ul>
        </nav>
        <nav role="navigation" aria-label="footer links dghweb">
            <ul class="footer-links small">
                <li hidden class="hide"><a href="https://depts.washington.edu/dghweb/" target="_blank"><?php _e('Site managed by DGHweb', 'uwdgh'); ?></a></li>
                <li><?php UWDGH::uwdgh_site_login_link(); ?></li>
            </ul>
        </nav>

        <p>&copy; <?php echo date("Y"); ?> <?php _e('University of Washington', 'uwdgh'); ?>  |  Seattle, WA</p>

    </div>

    </div><!-- #uw-container-inner -->
    </div><!-- #uw-container -->

<?php wp_footer(); ?>

</body>
</html>
