<?php the_date('F j, Y', '<p class="date">', '</p>'); ?>
<h2 style="font-size: 27px;">
  <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title() ?></a>
</h2>
<?php
if (get_option('show_byline_on_posts')) :
?>
  <div class="author-info">
  <?php $byline_author = esc_attr( get_post_meta(get_the_ID(), 'byline_author', true) ); ?>
  <?php if ( $byline_author ) : ?>
    By <?php echo $byline_author; ?>
  <?php else: ?>
    <?php the_author(); ?>
    <p class="author-desc"> <small><?php the_author_meta(); ?></small></p>
  <?php endif; ?>
  </div>
<?php
endif;
  if ( ! is_home() && ! is_search() && ! is_archive() ) :
    uw_mobile_menu();
  endif;
 if ( has_post_thumbnail() ) :
 	the_post_thumbnail( 'thumbnail' , 'style=margin-bottom:5px;');
 endif;
?>
<?php the_excerpt(); ?>
<?php
if ( get_option('uwdgh_theme_show_post_tax_terms') ) {
  get_template_part('uwdgh-tax-terms');
}
?>
<hr>
