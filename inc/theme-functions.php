<?php
/**
 * Contains function specific for this child-theme
 */

/* Add theme styles and scripts */
function globalhealthjustice_intranet_scripts_and_styles() {
   wp_register_script('globalhealthjustice', get_stylesheet_directory_uri() . '/js/globalhealthjustice.js', array('jquery'));
   wp_enqueue_script('globalhealthjustice');

  wp_register_script('flickity', get_stylesheet_directory_uri() . '/vendor/flickity/flickity.pkgd.min.js', array('jquery'));
  wp_enqueue_script('flickity');
  wp_enqueue_style( 'flickity', get_stylesheet_directory_uri() . '/vendor/flickity/flickity.css' );
}
add_action( 'wp_enqueue_scripts', 'globalhealthjustice_intranet_scripts_and_styles');

/* unregister Quick links menu location */
function globalhealthjustice_unregister_quicklinks_menu() {
  unregister_nav_menu( 'quick-links' );
}
add_action( 'init', 'globalhealthjustice_unregister_quicklinks_menu' );

/**
* Custom excerpt function
*/
function globalhealthjustice_get_excerpt($length=50){
  $excerpt = get_the_content();
  $excerpt = preg_replace(" ([.*?])",'',$excerpt);
  $excerpt = strip_shortcodes($excerpt);
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $length);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
  $excerpt = $excerpt.'&hellip;';
  //$excerpt = $excerpt.'... <a href="'.get_the_permalink().'">more</a>';
  return $excerpt;
}
