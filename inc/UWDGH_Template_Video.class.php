<?php
/**
* Class with functionality for the template-video.php template
*/
if ( !class_exists( 'UWDGH_Template_Video' ) ) {

	class UWDGH_Template_Video {
				/**
		    * class initializaton
		    */
		    public static function init() {
		      // add metaabox
		      add_action( 'add_meta_boxes_page',  array( __CLASS__, 'add_meta_box' ));
		      // save metabox action
		      add_action( 'publish_page',  array( __CLASS__, 'save_custom_post_meta' ));
		      add_action( 'draft_page',  array( __CLASS__, 'save_custom_post_meta' ));
		      add_action( 'future_page',  array( __CLASS__, 'save_custom_post_meta' ));
		      add_action( 'pending_page',  array( __CLASS__, 'save_custom_post_meta' ));
		      add_action( 'private_page',  array( __CLASS__, 'save_custom_post_meta' ));
		    }

			  /**
			   * Add custom metaboxes
			   * @param $post
			   */
			  static function add_meta_box( $post ) {
			    // Get the page template post meta
			    $page_template = get_post_meta( $post->ID, '_wp_page_template', true );
			    // If the current page uses our specific
			    // template, then output our custom metabox
			    if ( 'templates/template-video-hero.php' == $page_template ) {
		          add_meta_box(
		            'uwdgh-video-shortcode-metabox', // Metabox HTML ID attribute
		            'Video Hero', // Metabox title
		            array( __CLASS__, 'display_video_hero_metabox' ), // callback name
		            'page', // post type
		            'side', // context (advanced, normal, or side)
		            'core' // priority (high, core, default or low)
		          );
			    }
			  }
				// Define the meta box form fields here
				static function display_video_hero_metabox() {
					global $post;

					// get all video/mp4 files
					$args = array(
				    'post_type' => 'attachment',
				    'numberposts' => -1,
				    'post_status' => null,
				    'post_parent' => null, // any parent
						'post_mime_type' => 'video/mp4'
				    );
					$attachments = get_posts($args);

					wp_nonce_field( 'video_hero_save', 'video_hero_nonce' );
					wp_nonce_field( 'video_hero_byline_save', 'video_hero_byline_nonce' );
					wp_nonce_field( 'video_hero_link_url_save', 'video_hero_link_url_nonce' );
					wp_nonce_field( 'video_hero_link_button_text_save', 'video_hero_link_button_text_nonce' );
					?>
					<label for="video_hero"><strong><?php _e('Video file', 'uwdgh'); ?></strong>:</label><br>
					<select name="video_hero" id="video_hero" class="postbox">
		        <option value="">Select a video file...</option>
						<?php
						if ($attachments) {
					    foreach ($attachments as $file) {
								echo "<option value=" . $file->ID . " " . selected( get_post_meta( $post->ID, 'video_hero', true ), $file->ID ) . ">" . get_the_title($file->ID) . "</option>";
					    }
						}
						?>
			    </select>
					<label for="video_hero_byline"><strong><?php _e('Video byline', 'uwdgh'); ?></strong>:</label><br>
					<input class="" type="text" name="video_hero_byline" id="video_hero_byline" value="<?php echo esc_attr( get_post_meta( $post->ID, 'video_hero_byline', true ) ); ?>" size="34" maxlength="255" /><br><br>
					<span><strong>Button</strong></span><br>
					<label for="video_hero_link_url"><?php _e('link URL', 'uwdgh'); ?>:</label><br>
					<input class="" type="text" name="video_hero_link_url" id="video_hero_link_url" value="<?php echo esc_attr( get_post_meta( $post->ID, 'video_hero_link_url', true ) ); ?>" size="34" maxlength="255" /><br>
					<label for="video_hero_link_button_text"><?php _e('link button text', 'uwdgh'); ?>:</label><br>
					<input class="" type="text" name="video_hero_link_button_text" id="video_hero_link_button_text" value="<?php echo esc_attr( get_post_meta( $post->ID, 'video_hero_link_button_text', true ) ); ?>" size="34" maxlength="16" placeholder="Read more" /><br><br>

					<?php
				}


			  // Save metabox POST values
			  static function save_custom_post_meta() {
			    global $post;
			    // Sanitize/validate post meta here, before calling update_post_meta()
			    if ( ! isset($_POST['video_hero_nonce'])){
			        return;
			    }
			    if ( ! wp_verify_nonce( $_POST['video_hero_nonce'], 'video_hero_save' ) ) {
			        return;
			    }
			    if ( ! isset($_POST['video_hero_byline_nonce'])){
			        return;
			    }
			    if ( ! wp_verify_nonce( $_POST['video_hero_byline_nonce'], 'video_hero_byline_save' ) ) {
			        return;
			    }
			    if ( ! isset($_POST['video_hero_link_url_nonce'])){
			        return;
			    }
			    if ( ! wp_verify_nonce( $_POST['video_hero_link_url_nonce'], 'video_hero_link_url_save' ) ) {
			        return;
			    }
			    if ( ! isset($_POST['video_hero_link_button_text_nonce'])){
			        return;
			    }
			    if ( ! wp_verify_nonce( $_POST['video_hero_link_button_text_nonce'], 'video_hero_link_button_text_save' ) ) {
			        return;
			    }

			    $_byline = sanitize_text_field( $_POST['video_hero_byline'] );
					$_link_button_text = sanitize_text_field( $_POST['video_hero_link_button_text'] );
			    update_post_meta( $post->ID, 'video_hero_byline', $_byline );
					update_post_meta( $post->ID, 'video_hero', $_POST['video_hero'] );
			    update_post_meta( $post->ID, 'video_hero_link_button_text', $_link_button_text );
			    update_post_meta( $post->ID, 'video_hero_link_url', $_POST['video_hero_link_url'] );
			  }

  }

	UWDGH_Template_Video::init();
}
