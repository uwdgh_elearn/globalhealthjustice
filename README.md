# Global Health child-theme

This theme requires the UW 2014 WordPress theme:
https://github.com/uweb/uw-2014

The UW Slideshow template that is part of this theme requires the uw-slideshow plugin:
https://github.com/uweb/uw-slideshow
